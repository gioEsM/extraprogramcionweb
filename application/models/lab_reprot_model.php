<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class lab_reprot_model extends CI_Model {

	public function getregistro(){
        $this->db->where("matricula","matricula");
        $resultado = $this->db->get("lab_reprot");
        return $resultado->result();
    }
    
    public function save ($data){
		return $this->db->insert("lab_reprot",$data);
  }
  
  public function getregistros($id_lab_re){
    $this->db->where("id_lab_re",$id_lab_re);
    $resultado = $this->db->get("lab_reprot");
	return $resultado->row();
  }


}
