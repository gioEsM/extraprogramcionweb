<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class subimagen_model extends CI_Model {


    public function construct() {
        parent::__construct();
    }
    
    function subir($NombreCla,$Acta,$Curp,$ComprobanteDo,$ComprobanteES,$Fotografia,$FotografiaAD,$FotoINS)
    {
        $data = array(
            'NombreCla' => $NombreCla,
            'Acta' => $Acta,
            'Curp' => $Curp,
            'ComprobanteDo' => $ComprobanteDo,
            'ComprobanteES' => $ComprobanteES,
            'Fotografia' => $	$Fotografia,
            'FotografiaAD' => $FotografiaAD,
            'FotoINS' => $FotoINS

        );
        return $this->db->insert('imagenes', $data);
    }
}