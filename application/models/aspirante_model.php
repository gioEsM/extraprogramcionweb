<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class aspirante_model extends CI_Model {

	public function getregistro(){
        $this->db->where("Adeptados","1");
        $resultado = $this->db->get("aspirantes");
        return $resultado->result();
    }
    
    public function save($data){
		return $this->db->insert("aspirantes",$data);
  }
  
  public function getregistros($idAspirantes){
    $this->db->where("idAspirantes",$idAspirantes);
    $resultado = $this->db->get("aspirantes");
		return $resultado->row();
  }

  public function update($idAspirantes,$data){
    $this->db->where("idAspirantes",$idAspirantes);
    return $this->db->update("aspirantes",$data);
    
  }

  
}
