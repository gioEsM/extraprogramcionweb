<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class calificacione_model extends CI_Model {

	public function getregistro(){
        $this->db->where("estado","1");
        $resultado = $this->db->get("cpcalificacion");
        return $resultado->result();
    }
    
    public function save($data){
		return $this->db->insert("cpcalificacion",$data);
  }
  
  public function getregistros($idCpcalificacion){
    $this->db->where("idCpcalificacion",$idCpcalificacion);
    $resultado = $this->db->get("cpcalificacion");
		return $resultado->row();
  }

  public function update($idCpcalificacion,$data){
    $this->db->where("idCpcalificacion",$idCpcalificacion);
    return $this->db->update("cpcalificacion",$data);
    
  }

  
}
