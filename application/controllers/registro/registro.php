<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class registro extends CI_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model("registro_model");
	}

	
	public function index()
	{
        $data = array(
            'persona_docentes' => $this->registro_model->getregistro(),
        );
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/contenido/registros",$data);
		$this->load->view("layouts/footer");

    }
    public function add(){

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/contenido/add");
		$this->load->view("layouts/footer");
    }
    
    public function store(){
		$Nombre = $this->input->post("Nombre");
        $ApellidoP = $this->input->post("ApellidoP");
        $ApellidoM = $this->input->post("ApellidoM");
        $Ciudad = $this->input->post("Ciudad");
        $direccion = $this->input->post("direccion");
        $telefono = $this->input->post("telefono");
        $fecha_N = $this->input->post("fecha_N");
        $sexo = $this->input->post("sexo");
        $Tipo = $this->input->post("Tipo");
        $matricula = $this->input->post("matricula");
        $estado = $this->input->post("estado");

		$data  = array(
			'Nombre' => $Nombre, 
            'ApellidoP' => $ApellidoP,
            'ApellidoM' => $ApellidoM,
            'Ciudad' => $Ciudad,
            'direccion' => $direccion,
            'fecha_N' => $fecha_N,
            'sexo' => $sexo,
            'Tipo' => $Tipo,
            'matricula' => $matricula,
			'estado' => "1"
		);

		if ($this->registro_model->save($data)) {
			redirect(base_url()."registro/registro");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."registro/registro/add");
		}
  }

  public function edit($id_persona_docente){

    $data = array(
      'persona_docentes' => $this->registro_model->getregistros($id_persona_docente),);
  
    $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/contenido/edit",$data);
		$this->load->view("layouts/footer");
  }
  
  public function update(){
        $id_persona_docente=$this->input->post("idedit");
        $Nombre = $this->input->post("Nombre");
        $ApellidoP = $this->input->post("ApellidoP");
        $ApellidoM = $this->input->post("ApellidoM");
        $Ciudad = $this->input->post("Ciudad");
        $direccion = $this->input->post("direccion");
        $telefono = $this->input->post("telefono");
        $fecha_N = $this->input->post("fecha_N");
        $sexo = $this->input->post("sexo");
        $Tipo = $this->input->post("Tipo");
        $matricula = $this->input->post("matricula");
      

		$data  = array(
            'Nombre' => $Nombre, 
            'ApellidoP' => $ApellidoP,
            'ApellidoM' => $ApellidoM,
            'Ciudad' => $Ciudad,
            'direccion' => $direccion,
            'fecha_N' => $fecha_N,
            'sexo' => $sexo,
            'Tipo' => $Tipo,
            'matricula' => $matricula
		);

		if ($this->registro_model->update($id_persona_docente,$data)) {
			redirect(base_url()."registro/registro");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."registro/registro/add");
		}

  }
  public function delete($id){
    $data = array(
      'estado' => "0",
    );
    $this->registro_model->update($id,$data);
    echo "registro/registro";
  }
}

