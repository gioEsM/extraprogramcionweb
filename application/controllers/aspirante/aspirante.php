<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class aspirante extends CI_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model("aspirante_model");
	}

	
	public function index()
	{
        $data = array(
            'aspirantes' => $this->aspirante_model->getregistro(),
        );
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/AspiranteC/aspirante",$data);
		$this->load->view("layouts/footer");

    }
    public function add(){

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/AspiranteC/add");
		$this->load->view("layouts/footer");
    }
    
    public function store(){
		    $Nombre = $this->input->post("Nombre");
        $apellidoP = $this->input->post("apellidoP");
        $apellidoM = $this->input->post("apellidoM");
        $fechaN = $this->input->post("fechaN");
        $trabajas = $this->input->post("trabajas");
        $ocupacion = $this->input->post("ocupacion");
        $sexo = $this->input->post("sexo");
        $estadoN = $this->input->post("estadoN");
        $curp = $this->input->post("curp");
        $codigoP = $this->input->post("codigoP");
        $ciudadPo = $this->input->post("ciudadPo");
        $colonia = $this->input->post("colonia");
        $calle = $this->input->post("calle");
        $numEx	 = $this->input->post("numEx");
        $numIn = $this->input->post("numIn");
        $estado = $this->input->post("estado");
        $municipio = $this->input->post("municipio");
        $beca = $this->input->post("beca");
        $nombre_P = $this->input->post("nombre_P");
        $ocupacion_P = $this->input->post("ocupacion_P");
        $nombre_M = $this->input->post("nombre_M");
        $ocupacion_M = $this->input->post("ocupacion_M");

        $escuela_proc = $this->input->post("escuela_proc");
        $propedeutico = $this->input->post("propedeutico");
        $ano_bachillerato = $this->input->post("ano_bachillerato");
        $promedio_bachi = $this->input->post("promedio_bachi");
        $carrera_ut = $this->input->post("carrera_ut");
        $carrera_cur = $this->input->post("carrera_cur");
        $carrera_solicitada	 = $this->input->post("carrera_solicitada");
        $carrera_solicitada2 = $this->input->post("carrera_solicitada2");
        $medio_dif_upp = $this->input->post("medio_dif_upp");
        $telefono = $this->input->post("telefono");
        $correo_ele = $this->input->post("correo_ele");

        $Lengua_indigina = $this->input->post("Lengua_indigina");
        $dacapacidad = $this->input->post("dacapacidad");
        $telefono = $this->input->post("telefono");
        $Adeptados = $this->input->post("Adeptados");




        
       

		$data  = array(
			      'Nombre' => $Nombre, 
            'apellidoP' => $apellidoP,
            'apellidoM' => $apellidoM,
            'fechaN' => $fechaN,
            'trabajas' => $trabajas,
            'ocupacion' => $ocupacion,
            'sexo' => $sexo,
            'estadoN' => $estadoN,
            'curp' => $curp,
            'codigoP' => $codigoP,
            'ciudadPo' => $ciudadPo,
            'colonia' => $colonia,
            'calle' => $calle,
            'numEx' => $numEx,
            'numIn' => $numIn,
            'estado' => $estado,
            'municipio' => $municipio,
            'beca' => $beca,
            'nombre_P' => $nombre_P,
            'ocupacion_P' => $ocupacion_P,

            'nombre_M' => $nombre_M,
            'ocupacion_M' => $ocupacion_M,
            'escuela_proc' => $escuela_proc,
            'propedeutico' => $propedeutico,
            'ano_bachillerato' => $ano_bachillerato,
            'promedio_bachi' => $promedio_bachi,
            'carrera_ut' => $carrera_ut,
            'carrera_cur' => $carrera_cur,
            'carrera_solicitada' => $carrera_solicitada,
            'carrera_solicitada2' => $carrera_solicitada2,
            'medio_dif_upp' => $medio_dif_upp,
            'telefono' => $telefono,
            'correo_ele' => $correo_ele,
            'Lengua_indigina' => $Lengua_indigina,
            'dacapacidad' => $dacapacidad,

		      	'Adeptados' => "1"
		);

		if ($this->aspirante_model->save($data)) {
			redirect(base_url()."aspirante/aspirante");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."aspirante/aspirante/add");
		}
  }

  public function edit($idAspirantes){

    $data = array(
      'aspirante' => $this->aspirante_model->getregistros($idAspirantes),);
  
    $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/AspiranteC/edit",$data);
		$this->load->view("layouts/footer");
  }
  
  public function update(){
        $idAspirantes=$this->input->post("idedit");
        $Nombre = $this->input->post("Nombre");
        $apellidoP = $this->input->post("apellidoP");
        $apellidoM = $this->input->post("apellidoM");
        $fechaN = $this->input->post("fechaN");
        $trabajas = $this->input->post("trabajas");
        $ocupacion = $this->input->post("ocupacion");
        $sexo = $this->input->post("sexo");
        $estadoN = $this->input->post("estadoN");
        $curp = $this->input->post("curp");
        $codigoP = $this->input->post("codigoP");
        $ciudadPo = $this->input->post("ciudadPo");
        $colonia = $this->input->post("colonia");
        $calle = $this->input->post("calle");
        $numEx	 = $this->input->post("numEx");
        $numIn = $this->input->post("numIn");
        $estado = $this->input->post("estado");
        $municipio = $this->input->post("municipio");
        $beca = $this->input->post("beca");
        $nombre_P = $this->input->post("nombre_P");
        $ocupacion_P = $this->input->post("ocupacion_P");
        $nombre_M = $this->input->post("nombre_M");
        $ocupacion_M = $this->input->post("ocupacion_M");
        $escuela_proc = $this->input->post("escuela_proc");
        $propedeutico = $this->input->post("propedeutico");
        $ano_bachillerato = $this->input->post("ano_bachillerato");
        $promedio_bachi = $this->input->post("promedio_bachi");
        $carrera_ut = $this->input->post("carrera_ut");
        $carrera_cur = $this->input->post("carrera_cur");
        $carrera_solicitada	 = $this->input->post("carrera_solicitada");
        $carrera_solicitada2 = $this->input->post("carrera_solicitada2");
        $medio_dif_upp = $this->input->post("medio_dif_upp");
        $telefono = $this->input->post("telefono");
        $correo_ele = $this->input->post("correo_ele");
        $Lengua_indigina = $this->input->post("Lengua_indigina");
        $dacapacidad = $this->input->post("dacapacidad");
        $telefono = $this->input->post("telefono");
        

		$data  = array(
      'Nombre' => $Nombre, 
      'apellidoP' => $apellidoP,
      'apellidoM' => $apellidoM,
      'fechaN' => $fechaN,
      'trabajas' => $trabajas,
      'ocupacion' => $ocupacion,
      'sexo' => $sexo,
      'estadoN' => $estadoN,
      'curp' => $curp,
      'codigoP' => $codigoP,
      'ciudadPo' => $ciudadPo,
      'colonia' => $colonia,
      'calle' => $calle,
      'numEx' => $numEx,
      'numIn' => $numIn,
      'estado' => $estado,
      'municipio' => $municipio,
      'beca' => $beca,
      'nombre_P' => $nombre_P,
      'ocupacion_P' => $ocupacion_P,
      'nombre_M' => $nombre_M,
      'ocupacion_M' => $ocupacion_M,
      'escuela_proc' => $escuela_proc,
      'propedeutico' => $propedeutico,
      'ano_bachillerato' => $ano_bachillerato,
      'promedio_bachi' => $promedio_bachi,
      'carrera_ut' => $carrera_ut,
      'carrera_cur' => $carrera_cur,
      'carrera_solicitada' => $carrera_solicitada,
      'carrera_solicitada2' => $carrera_solicitada2,
      'medio_dif_upp' => $medio_dif_upp,
      'telefono' => $telefono,
      'correo_ele' => $correo_ele,
      'Lengua_indigina' => $Lengua_indigina,
      'dacapacidad' => $dacapacidad
		);

		if ($this->aspirante_model->update($idAspirantes,$data)) {
			redirect(base_url()."aspirante/aspirante");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."aspirante/aspirante/add");
		}

  }
  public function delete($id){
    $data = array(
      'Adeptados' => "0",
    );
    $this->aspirante_model->update($id,$data);
    echo "aspirante/aspirante";
  }
}

