<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class calificacione extends CI_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model("calificacione_model");
	}

	
	public function index()
	{
        $data = array(
            'cpcalificacion' => $this->calificacione_model->getregistro(),
        );
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/Cpcalifi/calificacione",$data);
		$this->load->view("layouts/footer");

    }
    public function add(){

		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/Cpcalifi/add");
		$this->load->view("layouts/footer");
    }
    
    public function store(){
		$idCpcalificacion=$this->input->post("idedit");
        $Matricula = $this->input->post("Matricula");
        $Palcial1 = $this->input->post("Palcial1");
        $Palcial2 = $this->input->post("Palcial2");
        $Palcial3 = $this->input->post("Palcial3");
        $Palcial4 = $this->input->post("Palcial4");
        $CalificacionF = $this->input->post("CalificacionF");
        $idAceptados = $this->input->post("idAceptados");
        $estado = $this->input->post("estado");

		$data  = array(
            'Matricula' => $Matricula, 
            'Palcial1' => $Palcial1,
            'Palcial2' => $Palcial2,
            'Palcial3' => $Palcial3,
            'Palcial4' => $Palcial4,
            'CalificacionF' => $CalificacionF,
            'idAceptados' => $idAceptados,
			'estado' => "1"
		);

		if ($this->calificacione_model->save($data)) {
			redirect(base_url()."calificacione/calificacione");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."calificacione/calificacione/add");
		}
  }

  public function edit($idCpcalificacion){

    $data = array(
      'cpcalificacion' => $this->calificacione_model->getregistros($idCpcalificacion),);
  
    $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/Cpcalifi/edit",$data);
		$this->load->view("layouts/footer");
  }
  
  public function update(){
        $idCpcalificacion=$this->input->post("idedit");
        $Matricula = $this->input->post("Matricula");
        $Palcial1 = $this->input->post("Palcial1");
        $Palcial2 = $this->input->post("Palcial2");
        $Palcial3 = $this->input->post("Palcial3");
        $Palcial4 = $this->input->post("Palcial4");
        $CalificacionF = $this->input->post("CalificacionF");
        $idAceptados = $this->input->post("idAceptados");
       
      

		$data  = array(
            'Matricula' => $Matricula, 
            'Palcial1' => $Palcial1,
            'Palcial2' => $Palcial2,
            'Palcial3' => $Palcial3,
            'Palcial4' => $Palcial4,
            'CalificacionF' => $CalificacionF,
            'idAceptados' => $idAceptados
		);

		if ($this->calificacione_model->update($idCpcalificacion,$data)) {
			redirect(base_url()."calificacione/calificacione");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."calificacione/calificacione/add");
		}

  }
  public function delete($id){
    $data = array(
      'estado' => "0",
    );
    $this->calificacione_model->update($id,$data);
    echo "calificacione/calificacione";
  }
}
