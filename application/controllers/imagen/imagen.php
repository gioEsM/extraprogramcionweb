<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class imagen extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('subimagen_model');
		$this->load->helper('download');
        
	}

	public function index(){
		$data['error'] = "";
		$data['errorArch'] = "";
		$data['estado'] = "";
        $data['archivo'] = "";
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/imagen/subimagen",$data);
		$this->load->view("layouts/footer");
    }
	public function subirImagen(){
		$config['upload_path'] = './uploads/imagenes/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '20048';
        $config['max_width'] = '20048';
        $config['max_height'] = '20048';

        $this->load->library('upload',$config);

        if (!$this->upload->do_upload("fileImagen")) {
            $data['error'] = $this->upload->display_errors();
			$this->load->view("layouts/header");
		    $this->load->view("layouts/aside");
		    $this->load->view("admin/subir/subimagen",$data);
		    $this->load->view("layouts/footer");
        } else {

            $file_info = $this->upload->data();

            $this->crearMiniatura($file_info['file_name']);

            $NombreClave = $this->input->post('NombreCl');
            $Actan = $file_info['file_name'];
            $Curp = $file_info['file_name'];
            
            
            $subir = $this->mupload->subir($NombreCla,$Acta,$Curp,$ComprobanteDo,$ComprobanteES,$Fotografia,$FotografiaAD,$FotoINS);      
            $data['NombreCla'] = $NombreCla;
            $data['Acta'] = $Acta;
            $data['Curp'] = $Curp;
            $data['ComprobanteDo'] = $ComprobanteDo;
            $data['ComprobanteES'] = $ComprobanteES;
            $data['Fotografia'] = $Fotografia;
            $data['FotografiaAD'] = $FotografiaAD;
            $data['FotoINS'] = $FotoINS;

            

            $this->load->view("layouts/header");
		    $this->load->view("layouts/aside");
		    $this->load->view("admin/subir/subimagen",$data);
		    $this->load->view("layouts/footer");
            
        }
    }
    
    function crearMiniatura($filename){
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'uploads/imagenes/'.$filename;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['new_image']='uploads/imagenes/thumbs/';
        $config['thumb_marker']='';//captura_thumb.png
        $config['width'] = 150;
        $config['height'] = 150;
        $this->load->library('image_lib', $config); 
        $this->image_lib->resize();
    }

}