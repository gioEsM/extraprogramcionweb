
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Universidad Politécnica de Tlaxcala
                <small>UPTx</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                
                <div class="box box-solid">
                <img src="<?php echo base_url()?>assets/template/dist/img/uptx.jpg" class="center-block" alt="User Image">
                    <div class="box-body">
                    <h2 align="center">
                   Somos una institución con un fuerte compromiso social; 
                   actualmente contamos con seis programas educativos en Ingeniería: 
                   Biotecnología, Química, Financiera, Tecnologías de la Información, Industrial y Mecatrónica.
                   </h2>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
