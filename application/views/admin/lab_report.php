
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        RESPUESTA SOLICITUD
        <small></small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fecha</th>
                                    <th>Edificio</th>
                                    <th>Ladoratorio</th>
                                    <th>Descripción</th>
                                    <th>Respuesta</th>
                                    <th>Asignado</th>
                                    <th>Matricula</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($lab_reprot)):?>
                                    <?php foreach($lab_reprot as $lab_reprot):?>
                                        <tr>
                                            <td><?php echo $lab_reprot->Fecha;?></td>
                                            <td><?php echo $lab_reprot->edificio;?></td>
                                            <td><?php echo $lab_reprot->laboratorio;?></td>
                                            <td><?php echo $lab_reprot->descripcion;?></td>
                                            <td><?php echo $lab_reprot->respuesta;?></td>
                                            <td><?php echo $lab_reprot->asignado;?></td>
                                            <td><?php echo $lab_reprot->matricula;?></td>
                                            <td><?php echo $lab_reprot->no_pc;?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-view" data-toggle="modal" data-target="#modal-default" value="">
                                                        <span class="fa fa-search"></span>
                                                    </button>
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.final de la siction -->
</div>
<!-- /.content-wrapper -->

