
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Captural Calificaciones
        <small>Listado</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url();?>calificacione/calificacione/add"><span class="fa fa-plus"></span>Registra calificaciones</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Matricula</th>
                                    <th>Palcial1</th>
                                    <th>Palcial2</th>
                                    <th>Palcial3</th>
                                    <th>Palcial4</th>
                                    <th>CalificacionF</th>
                                   
                
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($calificacione)):?>
                                    <?php foreach($calificacione as $calificacione):?>
                                        <tr>
                                            <td><?php echo $calificacione->idCpcalificacion;?></td>
                                            <td><?php echo $calificacione->Matricula;?></td>
                                            <td><?php echo $calificacione->Palcial1;?></td>
                                            <td><?php echo $calificacione->Palcial2;?></td>
                                            <td><?php echo $calificacione->Palcial3;?></td>
                                            <td><?php echo $calificacione->Palcial4;?></td>
                                            <td><?php echo $calificacione->CalificacionF;?></td>
                                            
                                            
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-view" data-toggle="modal" data-target="#modal-default" value="">
                                                        <span class="fa fa-search"></span>
                                                    </button>
                                                    <a href="<?php echo base_url();?>aspirante/aspirante/edit/<?php echo $aspirantes->idAspirantes;?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                    <a href="<?php echo base_url();?>aspirante/aspirante/delete/<?php echo $aspirantes->idAspirantes;?>" class="btn btn-danger btn-remove"><span class="fa fa-remove"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Informacion de la Categoria</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
