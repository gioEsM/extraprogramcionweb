
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
    Profesor
        <small>Nuevo</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                                
                             </div>
                        <?php endif;?>
                        <form action="<?php echo base_url();?>registro/registro/store" method="POST">
                            <div class="form-group">
                                <label for="codigo">Nombre:</label>
                                <input type="text" class="form-control" id="Nombre" name="Nombre">
                            </div>
                            <div class="form-group">
                                <label for="nombre">ApellidoP:</label>
                                <input type="text" class="form-control" id="ApellidoP" name="ApellidoP">
                            </div>
                            <div class="form-group">
                                <label for="nombre">ApellidoM:</label>
                                <input type="text" class="form-control" id="ApellidoM" name="ApellidoM">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Ciudad:</label>
                                <input type="text" class="form-control" id="Ciudad" name="Ciudad">
                            </div>
                            <div class="form-group">
                                <label for="precio">Dirección:</label>
                                <input type="text" class="form-control" id="direccion" name="direccion">
                            </div>
                            <div class="form-group">
                                <label for="precio">Telefono:</label>
                                <input type="text" class="form-control" id="telefono" name="telefono">
                            </div>
                            <div class="form-group">
                                <label for="stock">Fecha Nacimiento:</label>
                                <input type="text" class="form-control" id="fecha_N" name="fecha_N">
                            </div>
                            <div class="form-group">
                                <label for="stock">Sexo:</label>
                                <input type="text" class="form-control" id="sexo" name="sexo">
                            </div>
                            <div class="form-group">
                                <label for="stock">Tipo:</label>
                                <input type="text" class="form-control" id="Tipo" name="Tipo">
                            </div>
                            <div class="form-group">
                                <label for="stock">Matricula:</label>
                                <input type="num" class="form-control" id="matricula" name="matricula">
                            </div>
                            <div class="form-group">
                                <label for="stock">estado:</label>
                                <input type="text" class="form-control" id="estado" name="estado">
                            </div>
                            <div class="form-group">
                                <label for="categoria">Categoria:</label>
                                
                            </div>
                            <div class="form-group">

                                <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                                
                            </div>
                        </form>

                        <button type="button" class="btn btn-block btn-danger btn-lg"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Regresar</font></font></button>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
