
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Productos
        <small>Nuevo</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                                
                             </div>
                        <?php endif;?>
                        <form action="<?php echo base_url();?>registro/registro/update" method="POST">
                        <input type="hider" value="<?php echo $persona_docentes->id_persona_docente;?>" name='idedit'>
                            <div class="form-group">
                                <label for="codigo">Nombre:</label>
                                <input type="text" class="form-control" id="Nombre" name="Nombre" value="<?php echo $persona_docentes->Nombre;?>">
                            </div>
                            <div class="form-group">
                                <label for="nombre">ApellidoP:</label>
                                <input type="text" class="form-control" id="ApellidoP" name="ApellidoP" value="<?php echo $persona_docentes->ApellidoP;?>">
                            </div>
                            <div class="form-group">
                                <label for="nombre">ApellidoM:</label>
                                <input type="text" class="form-control" id="ApellidoM" name="ApellidoM" value="<?php echo $persona_docentes->ApellidoM;?>">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Ciudad:</label>
                                <input type="text" class="form-control" id="Ciudad" name="Ciudad" value="<?php echo $persona_docentes->Ciudad;?>">
                            </div>
                            <div class="form-group">
                                <label for="precio">Direccion:</label>
                                <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $persona_docentes->direccion;?>">
                            </div>
                            <div class="form-group">
                                <label for="precio">Telefono:</label>
                                <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $persona_docentes->telefono;?>">
                            </div>
                            <div class="form-group">
                                <label for="stock">Fecha Nacimiento:</label>
                                <input type="text" class="form-control" id="fecha_N" name="fecha_N" value="<?php echo $persona_docentes->fecha_N;?>">
                            </div>
                            <div class="form-group">
                                <label for="stock">Sexo:</label>
                                <input type="text" class="form-control" id="sexo" name="sexo" value="<?php echo $persona_docentes->sexo;?>">
                            </div>
                            <div class="form-group">
                                <label for="stock">Tipo:</label>
                                <input type="text" class="form-control" id="Tipo" name="Tipo" value="<?php echo $persona_docentes->Tipo;?>">
                            </div>
                            <div class="form-group">
                                <label for="stock">Matricula:</label>
                                <input type="num" class="form-control" id="matricula" name="matricula" value="<?php echo $persona_docentes->matricula;?>">
                            </div>
                            
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
