
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Profesores
        <small>Listado</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url();?>registro/registro/add"><span class="fa fa-plus"></span> Registra Profesores</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>ApellidoP</th>
                                    <th>ApellidoM</th>
                                    <th>Ciudad</th>
                                    <th>Direccion</th>
                                    <th>Telefono</th>
                                    <th>Sexo</th>
                                    <th>Tipo</th>
                                    <th>Matricula</th>
                                    <th>opciones</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($persona_docentes)):?>
                                    <?php foreach($persona_docentes as $persona_docentes):?>
                                        <tr>
                                            <td><?php echo $persona_docentes->id_persona_docente;?></td>
                                            <td><?php echo $persona_docentes->Nombre;?></td>
                                            <td><?php echo $persona_docentes->ApellidoP;?></td>
                                            <td><?php echo $persona_docentes->ApellidoM;?></td>
                                            <td><?php echo $persona_docentes->Ciudad;?></td>
                                            <td><?php echo $persona_docentes->direccion;?></td>
                                            <td><?php echo $persona_docentes->telefono;?></td>
                                            <td><?php echo $persona_docentes->sexo;?></td>
                                            <td><?php echo $persona_docentes->Tipo;?></td>
                                            <td><?php echo $persona_docentes->matricula;?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-view" data-toggle="modal" data-target="#modal-default" value="">
                                                        <span class="fa fa-search"></span>
                                                    </button>
                                                    <a href="<?php echo base_url();?>registro/registro/edit/<?php echo $persona_docentes->id_persona_docente;?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                    <a href="<?php echo base_url();?>registro/registro/delete/<?php echo $persona_docentes->id_persona_docente;?>" class="btn btn-danger btn-remove"><span class="fa fa-remove"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Informacion de la Categoria</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
