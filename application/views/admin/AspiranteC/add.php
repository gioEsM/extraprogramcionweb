
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Aspirante
        <small>Nuevo</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                                
                             </div>
                        <?php endif;?>
                        <form action="<?php echo base_url();?>aspirante/aspirante/store" method="POST">
                            <div class="form-group">
                                <label for="codigo">NOMBRE:</label>
                                <input type="text" class="form-control" id="Nombre" name="Nombre">
                            </div>
                            <div class="form-group">
                                <label for="nombre">APELLIDO PATERNO:</label>
                                <input type="text" class="form-control" id="apellidoP" name="apellidoP">
                            </div>
                            <div class="form-group">
                                <label for="nombre"> APELLIDO MATERNO:</label>
                                <input type="text" class="form-control" id="apellidoM" name="apellidoM">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">FECHA DE NACIMIENTO:</label>
                                <input type="date" id="fechaN" name="fechaN" value="2020-07-22" min="1960-01-01" max="2020-12-31">
                            </div>
                            <div class="form-group">
                                <label for="precio">TRABAJA:</label>
                                <br>
                                <input type="radio" id="trabajas" name="trabajas" value="SI">SI 
                                <br>
                                <input type="radio" id="trabajas" name="trabajas" value="NO">NO
                                <br>
                            </div>
                            <div class="form-group">
                                <label for="stock">OCUPACIÓN:</label>
                                <input type="text" class="form-control" id="ocupacion" name="ocupacion">
                            </div>
                            <div class="form-group">
                                <label for="stock">SEXO:</label>
                                <br>
                                <input type="radio" id="sexo" name="sexo" value="Masculino">Masculino 
                                <br>
                                <input type="radio" id="sexo" name="sexo" value="Femenino">Femenino
                                <br>
                            </div>
                            <div class="form-group">
                                <label for="precio">ESTADO DE NACIMIENTO:</label>
                                <input type="text" class="form-control" id="estadoN" name="estadoN">
                            </div>
                            <div class="form-group">
                                <label for="stock">CURP:</label>
                                <input type="text" class="form-control" id="curp" name="curp">
                            </div>
                            <div class="form-group">
                                <label for="stock">CÓDIGO POSTAL:</label>
                                <input type="text" id="codigoP" name="codigoP" maxlength="6" size="6" tabindex="12" class="texto2">
                            </div>
                            <div class="form-group">
                                <label for="stock">CIUDAD O POBLACIÓN:</label>
                                <input type="text" class="form-control" id="ciudadPo" name="ciudadPo">
                            </div>
                            <div class="form-group">
                                <label for="stock">COLONIA:</label>
                                <input type="text" class="form-control" id="colonia" name="colonia">
                            </div>  

                            <div class="form-group">
                                <label for="stock">DOMICILIO (CALLE):</label>
                                <input type="text" class="form-control" id="calle" name="calle">
                            </div>
                            <div class="form-group">
                                <label for="stock">NÚMERO EXTERIOR:</label>
                                <input type="text" name="numEx" id="numEx" maxlength="50" size="9" tabindex="12" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="stock">NÚMERO INTERIOR:</label>
                                <input type="text" name="numIn" id="numIn" maxlength="50" size="9" tabindex="11" class="form-control">
                            </div>
                            --
                            <div class="form-group">
                                <label for="stock">ESTADO:</label>
                                <input type="text" class="form-control" id="estado" name="estado">
                            </div>
                            <div class="form-group">
                                <label for="stock">MUNICIPIO:</label>
                                <input type="text" class="form-control" id="municipio" name="municipio">
                            </div>

                            <div class="form-group">
                                <label for="stock"> BECA CON LA QUE CUENTA:</label>
                                <select name="beca" id="beca" tabindex="20" class="texto2">
                                <option value="NINGUNA">NINGUNA</option> 
                                <option value="OPORTUNIDADES">OPORTUNIDADES</option>
                                <option value="PRONABES">PRONABES</option>
                                <option value="EXCELENCIA ACADEMICA">EXCELENCIA ACADEMICA</option>
                                <option value="INSTITUCION PRIVADA">INSTITUCION PRIVADA</option>
                                <option value="FUNDACIÓN">FUNDACIÓN</option>
                                <option value="VARIAS">VARIAS</option>
                                <option value="OTRA">OTRA</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="stock">NOMBRE DEL PADRE:</label>
                                <input type="text" class="form-control" id="nombre_P" name="nombre_P">
                            </div>
                            <div class="form-group">
                                <label for="stock">OCUPACIÓN:</label>
                                <input type="text" class="form-control" id="ocupacion_P" name="ocupacion_P">
                            </div>
                            <div class="form-group">
                                <label for="stock">NOMBRE DE LA MADRE:</label>
                                <input type="text" class="form-control" id="nombre_M" name="nombre_M">
                            </div>
                            <div class="form-group">
                                <label for="stock">OCUPACIÓN:</label>
                                <input type="text" class="form-control" id="ocupacion_M" name="ocupacion_M">
                            </div>

                            <div class="form-group">
                                <label for="stock">ESCUELA DE PROCEDENCIA:</label>
                                <input type="text" class="form-control" id="escuela_proc" name="escuela_proc">
                            </div>

                            <div class="form-group">
                            <label for="stock">PROPEDÉUTICO:</label>
                            <select name="propedeutico" id="propedeutico" tabindex="20" class="texto2">
                            <option></option>
                            <option value="Exactas">Exactas</option>
                            <option value="Contables">Contables</option>
                            <option value="Biologicas">Biologicas</option>
                            <option value="Sociales">Sociales</option>
                            <option value="Bachillerato">Bachillerato General</option>
                            </select>
                            </div>

                            <div class="form-group">
                            <label for="stock">AÑO DE TERMINO DE ULTIMOS ESTUDIOS:</label>
                            <select name="ano_bachillerato" id="ano_bachillerato" tabindex="21" class="Estilo11">
	                        <option></option>
                            <option value="1980">1980</option>
                            <option value="1981">1981</option>
                            <option value="1982">1982</option>
                            <option value="1983">1983</option>
                            <option value="1984">1984</option>
                            <option value="1985">1985</option>
                            <option value="1986">1986</option>
                            <option value="1987">1987</option>
                            <option value="1988">1988</option>
                            <option value="1989">1989</option>
                            <option value="1990">1990</option>
                            <option value="1991">1991</option>
                            <option value="1992">1992</option>
                            <option value="1993">1993</option>
                            <option value="1994">1994</option>
                            <option value="1995">1995</option>
                            <option value="1996">1996</option>
                            <option value="1997">1997</option>
                            <option value="1998">1998</option>
                            <option value="1999">1999</option>
                            <option value="2000">2000</option>
                            <option value="2001">2001</option>
                            <option value="2002">2002</option>
                            <option value="2003">2003</option>
                            <option value="2004">2004</option>
                            <option value="2005">2005</option>
                            <option value="2006">2006</option>
                            <option value="2007">2007</option>
                            <option value="2008">2008</option>
                            <option value="2009">2009</option>
                            <option value="2010">2010</option>
                            <option value="2011">2011</option>
                            <option value="2012">2012</option>
                            <option value="2013">2013</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>  
                            </select>
                            </div>


                            <div class="form-group">
                            <label for="stock">PROMEDIO FINAL DE ULTIMOS ESTUDIOS:</label>
                            <select name="promedio_bachi" id="promedio_bachi" tabindex="21" class="Estilo11">
                            <option></option>
                            <option value="6">6</option>
                            <option value="6.1">6.1</option>
                            <option value="6.2">6.2</option>
                            <option value="6.3">6.3</option>
                            <option value="6.4">6.4</option>
                            <option value="6.5">6.5</option>
                            <option value="6.6">6.6</option>
                            <option value="6.7">6.7</option>
                            <option value="6.8">6.8</option>
                            <option value="6.9">6.9</option>
                            <option value="7">7</option>
                            <option value="7.1">7.1</option>
                            <option value="7.2">7.2</option>
                            <option value="7.3">7.3</option>
                            <option value="7.4">7.4</option>
                            <option value="7.5">7.5</option>
                            <option value="7.6">7.6</option>
                            <option value="7.7">7.7</option>
                            <option value="7.8">7.8</option>
                            <option value="7.9">7.9</option>
                            <option value="8">8</option>
                            <option value="8.1">8.1</option>
                            <option value="8.2">8.2</option>
                            <option value="8.3">8.3</option>
                            <option value="8.4">8.4</option>
                            <option value="8.5">8.5</option>
                            <option value="8.6">8.6</option>
                            <option value="8.7">8.7</option>
                            <option value="8.8">8.8</option>
                            <option value="8.9">8.9</option>
                            <option value="9">9</option>
                            <option value="9.1">9.1</option>
                            <option value="9.2">9.2</option>
                            <option value="9.3">9.3</option>
                            <option value="9.4">9.4</option>
                            <option value="9.5">9.5</option>
                            <option value="9.6">9.6</option>
                            <option value="9.7">9.7</option>
                            <option value="9.8">9.8</option>
                            <option value="9.9">9.9</option>
                            <option value="10">10</option>  
                            </select>
                            </div>

                            <div class="form-group">
                                <label for="stock">LLENAR SÓLO EN CASO DE SER EGRESADO DE UNA UNIVERSIDAD TECNOLÓGICA</label>
                                    <div class="form-group">
                                        <label for="stock">UT DE PROCEDENCIA::</label>
                                        <input type="text" class="form-control" id="carrera_ut" name="carrera_ut">
                                    </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="stock">	CARRERA CURSADA:</label>
                                <input type="text" class="form-control" id="carrera_cur" name="carrera_cur">
                            </div>

                            <div class="form-group">
                            <label for="stock"> CARRERA 1A. OPCIÓN:</label>
                            <select name="carrera_solicitada" id="carrera_solicitada" class="texto2" style="width:250px"><option></option>
                            <option value="INGENIERÍA MECATRÓNICA"> INGENIERÍA MECATRÓNICA</option>
                            <option value="INGENIERÍA INDUSTRIAL"> INGENIERÍA INDUSTRIAL</option>
                            <option value="INGENIERÍA QUÍMICA"> INGENIERÍA QUÍMICA</option>
                            <option value="INGENIERÍA FINANCIERA"> INGENIERÍA FINANCIERA</option>
                            <option value="INGENIERÍA EN BIOTECNOLOGÍA"> INGENIERÍA EN BIOTECNOLOGÍA</option>
                            <option value="INGENIERÍA EN TECNOLOGÍAS DE LA INFORMACIÓN"> INGENIERÍA EN TECNOLOGÍAS DE LA INFORMACIÓN</option>
                            <option value="INGENIERÍA EN SISTEMAS AUTOMOTRICES"> INGENIERÍA EN SISTEMAS AUTOMOTRICES</option>
                            </select>
                            </div>

                            <div class="form-group">
                            <label for="stock"> CARRERA 2A. OPCIÓN:</label>
                            <select name="carrera_solicitada2" id="carrera_solicitada2" class="texto2" style="width:250px"><option></option>
                            <option value="INGENIERÍA MECATRÓNICA"> INGENIERÍA MECATRÓNICA</option>
                            <option value="INGENIERÍA INDUSTRIAL"> INGENIERÍA INDUSTRIAL</option>
                            <option value="INGENIERÍA QUÍMICA"> INGENIERÍA QUÍMICA</option>
                            <option value="INGENIERÍA FINANCIERA"> INGENIERÍA FINANCIERA</option>
                            <option value="INGENIERÍA EN BIOTECNOLOGÍA"> INGENIERÍA EN BIOTECNOLOGÍA</option>
                            <option value="INGENIERÍA EN TECNOLOGÍAS DE LA INFORMACIÓN"> INGENIERÍA EN TECNOLOGÍAS DE LA INFORMACIÓN</option>
                            <option value="INGENIERÍA EN SISTEMAS AUTOMOTRICES"> INGENIERÍA EN SISTEMAS AUTOMOTRICES</option>
                            </select>
                            </div>

                            <div class="form-group">
                            <label for="stock">CONOCIO LA UNIVERSIDAD POR:</label>
                            <select name="medio_dif_upp" id="medio_dif_upp" class="texto2" style="width:250px">
                            <option></option>
                            <option value="Visita a la escuela">Visita a la escuela</option>
                            <option value="Tríptico">Tríptico</option>
                            <option value="Radio">Radio</option>
                            <option value="Televisión">Televisión</option>
                            <option value="Internet">Internet</option>
                            <option value="Periodico">Periodico</option>
                            <option value="Amigos, Familiares, Compañeros, Maestros, Alumnos">Amigos, Familiares, Compañeros, Maestros, Alumnos</option>
                            <option value="Otro">Otro</option>
                            </select>
                            </div>


                            <div class="form-group">
                                <label for="stock"></label>
                            </div>

                            <div class="form-group">
                                <label for="stock">TELÉFONO:</label>
                                <input type="text" name="telefono" id="telefono" maxlength="20" size="20" class="texto2">
                            </div>
                            <div class="form-group">
                                <label for="stock">CORREO ELECTRÓNICO:</label>
                                <input type="text" name="correo_ele" id="correo_ele" class="texto2" size="20" maxlength="50">
                            </div>
                            
                            <div class="form-group">
                            <label for="stock">LENGUA INDIGENA QUE HABLA:</label>
                            <select name="Lengua_indigina" id="Lengua_indigina" class="form-control">
	    	                <option value="Ninguna" title="">- Ninguna -</option>
                            <option value="Otras lenguas" title="">Otras lenguas</option>
                            <option value="Aguacateco" title="">Aguacateco</option>
                            <option value="Kiliwa" title="">Kiliwa (Ko-lew)</option>
                            <option value="Cochimí" title="">Cochimí (Laymón, mti-pá)</option>
                            <option value="Ixil" title="">Ixil</option>
                            <option value="Kikapú" title="">Kikapú (Kikapooa)</option>
                            <option value="Pápago" title="">Pápago (Tohono o-odam)</option>
                            <option value="Kumiai" title="">Kumiai (Ti-pai)</option>
                            <option value="Motocintleco" title="">Motocintleco (Qatok)</option>
                            <option value="Cucapá" title="">Cucapá (Es péi)</option>
                            <option value="Paipai" title="">Paipai (Akwa-ala)</option>
                            <option value="Kakchiquel" title="">Kakchiquel (K-akchikel)</option>
                            <option value="Quiché" title="">Quiché (Q-iché)</option>
                            <option value="Ixcateco" title="">Ixcateco</option>
                            <option value="Seri" title="">Seri (Cmiique iitom)</option>
                            <option value="Ocuilteco" title="">Ocuilteco (Tlahuica)</option>
                            <option value="Jacalteco" title="">Jacalteco (Abxubal)</option>
                            <option value="Lacandón" title="">Lacandón (Hach t-an)</option>
                            <option value="Kekchí" title="">Kekchí (K-ekchí)</option>
                            <option value="Pima" title="">Pima (O-odham)</option>
                            <option value="Chocho" title="">Chocho (Runixa ngiigua)</option>
                            <option value="Matlatzinca" title="">Matlatzinca (Botuná)</option>
                            <option value="Guarijío" title="">Guarijío (Warihío)</option>
                            <option value="Chichimeca" title="">Chichimeca jonaz (Uza)</option>
                            <option value="Tacuate" title="">Tacuate</option>
                            <option value="Chuj" title="">Chuj</option>
                            <option value="Chontal" title="">Chontal de Oaxaca (Slijuala sihanuk)</option>
                            <option value="Mame" title="">Mame (Qyool)</option>
                            <option value="Pame" title="">Pame (Xigüe)</option>
                            <option value="Tepehua" title="">Tepehua (Hamasipini)</option>
                            <option value="Kanjobal" title="">Kanjobal (K-anjobal)</option>
                            <option value="Yaqui" title="">Yaqui (Yoeme)</option>
                            <option value="Cuicateco" title="">Cuicateco (Nduudu yu)</option>
                            <option value="Huave" title="">Huave (Ikoods)</option>
                            <option value="Popoloca" title="">Popoloca</option>
                            <option value="Cora" title="">Cora (Nayeeri)</option>
                            <option value="Triqui" title="">Triqui (Xnanj nu´a, Stnanj ni´, Nanj ni´inj, Tnanj ni´inj)</option>
                            <option value="Tepehuano" title="">Tepehuano (O-dami)</option>
                            <option value="Mayo" title="">Mayo (Yoreme)</option>
                            <option value="Huichol" title="">Huichol (Wixárika)</option>
                            <option value="Chontal" title="">Chontal de Tabasco (Yokot t-an)</option>
                            <option value="Popoluca" title="">Popoluca (Tuncápxe)</option>
                            <option value="Tojolabal" title="">Tojolabal (Tojolwinik otik)</option>
                            <option value="Chatino" title="">Chatino (Cha-cña)</option>
                            <option value="Amuzgo" title="">Amuzgo (Tzañcue)</option>
                            <option value="Zoque" title="">Zoque (O-de püt)</option>
                            <option value="Tarahumara" title="">Tarahumara (Rarámuri)</option>
                            <option value="Tlapaneco" title="">Tlapaneco (Me-phaa)</option>
                            <option value="Mixe" title="">Mixe (Ayüük)</option>
                            <option value="Purépecha" title="">Purépecha (P-urhépecha)</option>
                            <option value="Mazahua" title="">Mazahua (Jñatio)</option>
                            <option value="Chinanteco" title="">Chinanteco (Tsa jujmí)</option>
                            <option value="Huasteco" title="">Huasteco (Téenek)</option>
                            <option value="Chol" title="">Chol (Ch-ol, Laktyan)</option>
                            <option value="Mazateco" title="">Mazateco (Ha shuta enima)</option>
                            <option value="Totonaco" title="">Totonaco (Tachiwin)</option
                            ><option value="Otomí" title="">Otomí (Hñähñü)</option>
                            <option value="Tzeltal" title="">Tzeltal (Batz-il K-op)</option>
                            <option value="Tzotzil" title="">Tzotzil (Batz-i k-op)</option>
                            <option value="Zapoteco" title="">Zapoteco (Binizaa)</option>
                            <option value="Mixteco" title="">Mixteco (Tu-un savi)</option>
                            <option value="Maya" title="">Maya (Maaya t-aan)</option>
                            <option value="Náhuatl" title="">Náhuatl (Nahuatlahtolli)</option>	
                            </select>
                            </div>


                            <div class="form-group">
                            <label for="stock">DISCAPACIDAD:</label>
                            <select name="dacapacidad" id="dacapacidad" class="form-control">
		                    <option value="NINGUNA" title="NINGUNA">NINGUNA</option>
                            <option value="RETRASO MENTAL" title="RETRASO MENTAL">RETRASO MENTAL</option>
                            <option value="SINDROME DE ASPENGER" title="SINDROME DE ASPENGER">SINDROME DE ASPENGER</option>
                            <option value="SINDROME DE DOWN" title="SINDROME DE DOWN">SINDROME DE DOWN</option>
                            <option value="AUTISMO" title="AUTISMO">AUTISMO</option>
                            <option value="DISCAPACIDAD COGNITIVA" title="DISCAPACIDAD COGNITIVA">DISCAPACIDAD COGNITIVA</option>
                            <option value="OTROS" title="OTROS">OTROS</option>
                            <option value="HIPOCONDRÍA" title="HIPOCONDRÍA">HIPOCONDRÍA</option>
                            <option value="TRASTORNO SOMATICO" title="TRASTORNO SOMATICO">TRASTORNO SOMATICO</option>
                            <option value="OBSESIONES" title="OBSESIONES">OBSESIONES</option>
                            <option value="FOBIA" title="FOBIA">FOBIA</option>
                            <option value="ESQUIZOFRENIA" title="ESQUIZOFRENIA">ESQUIZOFRENIA</option>
                            <option value="TRASTORNOS MENTALES ORGÁNICOS" title="TRASTORNOS MENTALES ORGÁNICOS">TRASTORNOS MENTALES ORGÁNICOS</option>
                            <option value="DISCAPACIDAD INTELECTUAL" title="DISCAPACIDAD INTELECTUAL">DISCAPACIDAD INTELECTUAL</option>
                            <option value="OTROS" title="OTROS">OTROS</option>
                            <option value="RETINITIS PIGMENTOSA" title="RETINITIS PIGMENTOSA">RETINITIS PIGMENTOSA</option>
                            <option value="MIOPIA CERCA" title="MIOPIA CERCA">MIOPIA CERCA</option>
                            <option value="RETINOPATÍA DIABÉTICA" title="RETINOPATÍA DIABÉTICA">RETINOPATÍA DIABÉTICA</option>
                            <option value="CATARATA" title="CATARATA">CATARATA</option>
                            <option value="MACULAR RELACIONADA CON LA EDAD" title="MACULAR RELACIONADA CON LA EDAD">MACULAR RELACIONADA CON LA EDAD</option>
                            <option value="VISUAL EN EL GLAUCOMA" title="VISUAL EN EL GLAUCOMA">VISUAL EN EL GLAUCOMA</option>
                            <option value="DISCAPACIDAD VISUAL" title="DISCAPACIDAD VISUAL">DISCAPACIDAD VISUAL</option>
                            <option value="OTROS" title="OTROS">OTROS</option>
                            <option value="LESIONES OSTEOARTICULARES" title="LESIONES OSTEOARTICULARES">LESIONES OSTEOARTICULARES</option>
                            <option value="REUMATISMOS INFANTILES" title="REUMATISMOS INFANTILES">REUMATISMOS INFANTILES</option>
                            <option value="MICROBIANAS" title="MICROBIANAS">MICROBIANAS</option>
                            <option value="DISTROFICAS" title="DISTROFICAS">DISTROFICAS</option>
                            <option value="MALFOMACIONES CONGÉNITAS" title="MALFOMACIONES CONGÉNITAS">MALFOMACIONES CONGÉNITAS</option>
                            <option value="DISCAPACIDAD FISICA O MOTORAS" title="DISCAPACIDAD FISICA O MOTORAS">DISCAPACIDAD FISICA O MOTORAS</option>		
                            </select>
                            </div>


                            <div class="form-group">
                                <label for="stock">	Adeptado:</label>
                                <input type="text" class="form-control" id="Adeptados" name="Adeptados">
                            </div>
                            <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                            </div>
                        </form>
                        <button type="button" class="btn btn-block btn-danger btn-lg" onClick="history.go(-1);"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Regresar</font></font></button>

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
