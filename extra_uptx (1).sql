-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-10-2020 a las 02:17:18
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `extra_uptx`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirantes`
--

CREATE TABLE `aspirantes` (
  `idAspirantes` int(11) NOT NULL,
  `Nombre` varchar(200) DEFAULT NULL,
  `apellidoP` varchar(30) DEFAULT NULL,
  `apellidoM` varchar(30) DEFAULT NULL,
  `fechaN` date DEFAULT NULL,
  `trabajas` varchar(20) DEFAULT NULL,
  `ocupacion` varchar(110) DEFAULT NULL,
  `sexo` varchar(4) DEFAULT NULL,
  `estadoN` varchar(110) DEFAULT NULL,
  `curp` varchar(20) DEFAULT NULL,
  `codigoP` int(5) DEFAULT NULL,
  `ciudadPo` varchar(90) DEFAULT NULL,
  `colonia` varchar(90) DEFAULT NULL,
  `calle` varchar(50) DEFAULT NULL,
  `numEx` varchar(10) DEFAULT NULL,
  `numIn` varchar(10) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `municipio` varchar(300) DEFAULT NULL,
  `beca` varchar(110) DEFAULT NULL,
  `nombre_P` varchar(200) DEFAULT NULL,
  `ocupacion_P` varchar(50) DEFAULT NULL,
  `nombre_M` varchar(200) NOT NULL,
  `ocupacion_M` varchar(50) DEFAULT NULL,
  `escuela_proc` varchar(200) DEFAULT NULL,
  `propedeutico` varchar(30) DEFAULT NULL,
  `ano_bachillerato` year(4) DEFAULT NULL,
  `promedio_bachi` float(6,2) DEFAULT NULL,
  `carrera_ut` varchar(50) DEFAULT NULL,
  `carrera_cur` varchar(200) DEFAULT NULL,
  `carrera_solicitada` varchar(50) DEFAULT NULL,
  `carrera_solicitada2` varchar(50) DEFAULT NULL,
  `medio_dif_upp` varchar(246) DEFAULT NULL,
  `telefono` int(13) DEFAULT NULL,
  `correo_ele` varchar(50) DEFAULT NULL,
  `Lengua_indigina` varchar(245) DEFAULT NULL,
  `dacapacidad` varchar(245) DEFAULT NULL,
  `Adeptados` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aspirantes`
--

INSERT INTO `aspirantes` (`idAspirantes`, `Nombre`, `apellidoP`, `apellidoM`, `fechaN`, `trabajas`, `ocupacion`, `sexo`, `estadoN`, `curp`, `codigoP`, `ciudadPo`, `colonia`, `calle`, `numEx`, `numIn`, `estado`, `municipio`, `beca`, `nombre_P`, `ocupacion_P`, `nombre_M`, `ocupacion_M`, `escuela_proc`, `propedeutico`, `ano_bachillerato`, `promedio_bachi`, `carrera_ut`, `carrera_cur`, `carrera_solicitada`, `carrera_solicitada2`, `medio_dif_upp`, `telefono`, `correo_ele`, `Lengua_indigina`, `dacapacidad`, `Adeptados`) VALUES
(1, 'sdjghjk', 'muñoz', 'ma', '2000-05-22', 'NO', 'fgfb', 'Feme', 'nc', 'MUMA751115MTLXRL07', 90830, 'tla', 'fsa', 'sfs', '2', '2', 'tad', 'sfaa', 'PRONABES', 'sfs', 'sfs', 'sfsf', '', 'dfd', '', 0000, 0.00, 'dfd', 'dfd', '', '', '', 23456789, 'sdgsdg', 'Ninguna', 'NINGUNA', 1),
(2, 'saul', '', '', '2020-07-22', NULL, '', NULL, '', '', 0, '', '', '', '', '', '', '', 'NINGUNA', '', '', '', '', '', '', 0000, 0.00, '', '', '', '', '', 0, '', 'Ninguna', 'NINGUNA', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(2) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `descripcion` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `descripcion`) VALUES
(1, 'superadmin', NULL),
(2, 'admin', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(4) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidoP` varchar(20) NOT NULL,
  `apellidoM` varchar(20) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `rol_id` int(2) NOT NULL,
  `estado` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `nombre`, `apellidoP`, `apellidoM`, `username`, `password`, `rol_id`, `estado`) VALUES
(1, 'Giovanny', 'Muñoz', 'martinez', 'gio', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1, 1),
(2, 'w', 'q', 'w', 'q', 'w', 2, 1),
(3, 'Giovanny', 'Muñoz', 'Martinez', 'Gio', '1234', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aspirantes`
--
ALTER TABLE `aspirantes`
  ADD PRIMARY KEY (`idAspirantes`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nombre` (`nombre`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `rol_id` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aspirantes`
--
ALTER TABLE `aspirantes`
  MODIFY `idAspirantes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
